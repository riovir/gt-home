# gt-home

> GT-Home is a simple, privacy respecting overtime tracker.

Accessed as a Progressive Web App [hosted by Netlify](https://gt-home.riovir.com/). It remembers (in your browser's localStorage) how many minutes of over-/ under-time you have. For added convenience you can choose to set your usual working hours, in which case the app attempts to guess how many minutes to add / subtract. It does so simply by assuming that you just arrived, or about to leave. (Whichever makes more sense.)

The application uses no data server by default, and really doesn't want to peek into your private life.