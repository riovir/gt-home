const path = require('path');

module.exports = {
	name: 'Get home!',
	short_name: 'GtHome',
	description: "Know when it's time to go home.",
	background_color: '#252525',
	theme_color: '#121212',
	ios: true,
	icons: [
		{
			src: path.resolve(__dirname, '../src/assets/beach.png'),
			sizes: [36, 48, 72, 96, 144, 192, 512],
			ios: true,
		},
	],
};
