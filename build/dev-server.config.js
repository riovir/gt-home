module.exports = function DevServerConfig({ publicPath = '/' } = {}) {
	return {
		historyApiFallback: { index: publicPath },
		compress: true,
		hot: true,
		port: 3000,
	};
};
