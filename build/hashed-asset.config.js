const hash = process.env.NODE_ENV === 'production' ? '[contenthash]' : '[hash]';

module.exports.jsAssetName = `assets/[name]-${hash}.js`;
module.exports.cssAssetName = `assets/[name]-${hash}.css`;
module.exports.genericAssetName = `assets/[name]-${hash}.[ext]`;
