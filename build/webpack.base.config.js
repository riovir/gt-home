const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');
const { cssAssetName, genericAssetName } = require('./hashed-asset.config');

const cssLoaders = [CssExtractPlugin.loader, withSourceMap('css-loader'), withSourceMap('postcss-loader')];
const babelInclude = projectPaths('src', 'test');

module.exports = {
	resolve: {
		alias: {
			'src': resolve(__dirname, '../src'),
			'test': resolve(__dirname, '../test'),
		},
	},
	module: {
		rules: [
			{ test: /\.js$/, loader: 'babel-loader', include: babelInclude },
			{ test: /\.(scss|sass)$/, include: /main\.scss$/, use: [...cssLoaders, 'resolve-url-loader', withSourceMap('sass-loader')] },
			{ test: /\.(scss|sass)$/, exclude: /main\.scss$/, use: ['raw-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader'] },
			{
				test: /\.woff(2)?$/,
				loader: 'url-loader',
				options: { limit: 10000, mimetype: 'application/font-woff', name: genericAssetName },
			},
			{ test: /\.svg$/, loader: 'file-loader', options: { name: genericAssetName } },
			{
				test: /\.png$/,
				loader: 'url-loader',
				options: { limit: 10000, mimetype: 'image/png', name: genericAssetName },
			},
		],
	},
	plugins: [
		new CssExtractPlugin({ filename: cssAssetName }),
		new HtmlWebpackPlugin({ template: 'src/index.html', favicon: 'src/assets/beach.png', chunksSortMode: 'none' }),
	],
	devtool: 'source-map',
};

function withSourceMap(loader) {
	return { loader, options: { sourceMap: true } };
}

function projectPaths(...rootPaths) {
	const toAbsolute = rootRelative => resolve(__dirname, '../', rootRelative);
	return rootPaths.map(toAbsolute);
}
