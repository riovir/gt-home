const path = require('path');
const { DefinePlugin, HotModuleReplacementPlugin } = require('webpack');
const { merge } = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const OfflinePlugin = require('offline-plugin');
const { jsAssetName } = require('./hashed-asset.config');
const pwaManifest = require('./pwa.manifest');
const baseConfig = require('./webpack.base.config');
const DevServerConfig = require('./dev-server.config');
const { config } = require('../package');

const publicPath = process.env.PUBLIC_PATH || config.default_public_path;
const publicHost = process.env.PUBLIC_HOST || config.default_public_host;
const distPath = path.resolve(__dirname, '../dist');

module.exports = merge(baseConfig, {
	entry: {
		app: './src/main.js',
	},
	output: {
		path: distPath,
		publicPath,
		filename: jsAssetName,
	},
	node: false,
	optimization: {
		moduleIds: 'hashed',
		runtimeChunk: 'single',
		splitChunks: { chunks: 'all' },
	},
	plugins: [
		new CleanWebpackPlugin(),
		new DefinePlugin({
			'process.env.PUBLIC_PATH': JSON.stringify(publicPath),
			'process.env.PUBLIC_HOST': JSON.stringify(publicHost),
		}),
		new CopyWebpackPlugin({ patterns: [{ from: './static' }] }),
		new WebpackPwaManifest(pwaManifest),
		new OfflinePlugin({
			appShell: '/',
			autoUpdate: 1000 * 60 * 2,
			excludes: ['_redirects', '_headers', '**/*.map'],
			responseStrategy: process.env.NODE_ENV === 'production' ?
				'cache-first' :
				'network-first',
			updateStrategy: 'changed',
			ServiceWorker: { events: true, navigateFallbackURL: '/' },
		}),
		...conditionally(process.env.ANALYZE, new BundleAnalyzerPlugin({
			analyzerMode: 'static',
			generateStatsFile: true,
		})),
		...conditionally(process.env.NODE_ENV !== 'production', new HotModuleReplacementPlugin()),
	],
	mode: process.env.NODE_ENV === 'development' ? 'development' : 'production',
	devServer: DevServerConfig({ publicPath }),
});

function conditionally(condition, value) {
	return condition ? [].concat(value) : [];
}
