export function render({ render }, host = {}) {
	const el = document.createElement('div');
	const update = render(host);
	update(host, el);
	return { el, update };
}
