global.requestAnimationFrame = (fn) => Promise.resolve().then(fn);

process.env.PUBLIC_HOST = 'https://test-server.com';
console.error = jest.fn();
