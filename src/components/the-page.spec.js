import { render } from 'test/utils';
import { ThePage } from './the-page';

test('docTitle: is translated', () => {
	const host = setup({ translations: { title: 'test' } });
	expect(ThePage.docTitle.get(host)).toBe('test');
});

test('docTitle: is synced to document.title', () => {
	ThePage.docTitle.observe(null, 'test-title');
	expect(document.title).toBe('test-title');
});

test('metaDescription: is translated', () => {
	const host = setup({ translations: { description: 'test' } });
	expect(ThePage.metaDescription.get(host)).toBe('test');
});

test('metaDescription: is synced to document.title', () => {
	const meta = addedToHead(Element('meta', { name: 'description' }));
	ThePage.metaDescription.observe(null, 'test-description');
	expect(meta.getAttribute('content')).toBe('test-description');
});

test('renders translated footer as inline HTML', () => {
	const host = setup({ translations: {
		'html.footer': '<em>test</em>' },
	});
	const { el } = render(ThePage, host);
	expect(el.querySelector('footer').innerHTML)
			.toContain('<em>test</em>');
});

function setup({
	translations = {},
} = {}) {
	const $t = key => translations[key] || `translation of ${key}`;
	return { $t };
}

function Element(tag, attrs = {}) {
	const el = document.createElement(tag);
	Object.entries(attrs).forEach(([key, value]) => el.setAttribute(key, value));
	return el;
}

function addedToHead(el) {
	document.head.appendChild(el);
	return el;
}
