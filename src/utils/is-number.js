export function isNumber(value) {
	return !isNaN(value) && typeof value === 'number';
}
