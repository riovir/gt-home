import { guessOvertime } from './guess-overtime';

const AT_10 = timeOf('10:00');
const AT_18 = timeOf('18:00');
const AT_01 = timeOf('01:00');
const AT_23 = timeOf('23:00');

const AT_00_30 = timeOf('00:30');
const AT_10_15 = timeOf('10:15');
const AT_17_45 = timeOf('17:45');
const AT_18_30 = timeOf('18:30');
const AT_23_45 = timeOf('23:45');

test('guesses in minutes', () => {
	expect(guessOvertime({
		workingUntilMs: AT_18,
		timeMs: AT_18_30,
	})).toEqual(30);
});

describe('when working hours are set', () => {
	test('uses closest interval entry', () => {
		const hours = { workingFromMs: AT_10, workingUntilMs: AT_18 };
		expect(guessOvertime({ ...hours, timeMs: AT_10_15 })).toEqual(-15);
		expect(guessOvertime({ ...hours, timeMs: AT_17_45 })).toEqual(-15);
		expect(guessOvertime({ ...hours, timeMs: AT_18_30 })).toEqual(30);
	});

	test('considers next day', () => {
		const hours = { workingFromMs: AT_01, workingUntilMs: AT_18 };
		expect(guessOvertime({ ...hours, timeMs: AT_23_45 })).toEqual(75);
	});

	test('considers previous day', () => {
		const hours = { workingFromMs: AT_10, workingUntilMs: AT_23 };
		expect(guessOvertime({ ...hours, timeMs: AT_00_30 })).toEqual(90);
	});

	test('accepts ms values larger than a day', () => {
		const hours = {
			workingFromMs: timeOf('10:00', 2000),
			workingUntilMs: timeOf('18:00', 1999),
		};
		const timeMs = timeOf('10:15', 2020);
		expect(guessOvertime({ ...hours, timeMs })).toEqual(-15);
	});
});

test('rounds guess according to roundTo option', () => {
	expect(guessOvertime({
		workingFromMs: AT_10,
		timeMs: timeOf('10:07:30'),
		roundTo: 5,
	})).toEqual(-5);
});

test('returns 0 without entries', () => {
	expect(guessOvertime({ timeMs: 999 })).toBe(0);
});

test('throws error on invalid time', () => {
	expect(() => guessOvertime()).toThrow();
	expect(() => guessOvertime({ timeMs: 'not a number' })).toThrow();
	expect(() => guessOvertime({ workingFromMs: 'not a number' })).toThrow();
	expect(() => guessOvertime({ workingUntilMs: 'not a number' })).toThrow();
});

function timeOf(time, year = 1970) {
	return new Date(`${year}-01-01T${time}`).getTime();
}
