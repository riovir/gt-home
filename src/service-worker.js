import { install, applyUpdate } from 'offline-plugin/runtime';

export function installServiceWorker({ onUpdated }) {
	return install({
		onUpdated,
		onUpdateReady: () => { applyUpdate(); },
	});
}
