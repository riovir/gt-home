import { CurveFunction as Curve } from './curve-function';

test('returns x when x = 0', () => expect(Curve()(0) === 0).toBe(true));
test('returns x when x = range', () => expect(Curve({ range: 42 })(42)).toBe(42));

describe('with easing = 1', () => {
	const curve = Curve({ easing: 1 });

	test('keeps negative values negative', () => expect(curve(-10)).toBe(-10));
	test('snaps return to nearest step location', () => expect(Curve({ easing: 1, step: 4 })(3)).toBe(4));
	test('is linear', () => {
		const expectSameValue = value => expect(curve(value)).toBe(value);
		[1, 2, 3, 100, 9999].forEach(expectSameValue);
	});
});

describe('with easing = 2', () => {
	const curve = Curve({ easing: 2 });

	test('makes small value smaller', () => expect(curve(10)).toBeLessThan(10));
	// XXX: This is not really intended
	test('makes vale close to range still smaller', () => expect(curve(90)).toBeLessThan(90));
});

describe('with non-integer easing', () => {
	const curve = Curve({ easing: 2.75, step: 1 });

	test('can handle positive number', () => expect(curve(50)).toBe(15));
	test('can handle negative number', () => expect(curve(-50)).toBe(-15));
});
