import { render } from 'test/utils';
import { BNavbarItem } from './b-navbar-item';

test('renders anchor tag when href is set', () => {
	const { el } = render(BNavbarItem, { href: '/url' });
	expect(el.querySelector('a[href="/url"]')).toBeTruthy();
});

test('renders div with falsy href', () => {
	const { el } = render(BNavbarItem);
	expect(el.querySelector('a')).toBeFalsy();
	expect(el.querySelector('div')).toBeTruthy();
});
