import { render } from 'test/utils';
import { BNavbar, toggleMenu } from './b-navbar';

describe('accessibility', () => {
	test('ariaLabel: sets aria-label on nav element', () => {
		const { nav } = setup({ ariaLabel: 'test' });
		expect(nav.getAttribute('aria-label')).toBe('test');
	});

	test('menuId: has a truthy value by default', () => {
		expect(BNavbar.menuId()).toBeTruthy();
	});

	test('menuId: connects navbar-menu and navbar-burger', () => {
		const { burger, menu } = setup({ menuId: 'test' });
		expect(burger.getAttribute('data-target')).toBe(menu.id);
	});

	test('burgerAriaLabel: sets aria-label on burger', () => {
		const { burger } = setup({ burgerAriaLabel: 'test' });
		expect(burger.getAttribute('aria-label')).toBe('test');
	});

	test('menuActive: sets aria-expanded on burger', () => {
		const { burger: burger1 } = setup({ menuActive: true });
		expect(burger1.getAttribute('aria-expanded')).toBe('true');
		const { burger: burger2 } = setup({ menuActive: false });
		expect(burger2.getAttribute('aria-expanded')).toBe('false');
	});
});

test('menuActive: controls is-active on menu', () => {
	const { menu: menu1 } = setup({ menuActive: true });
	expect(menu1.classList).toContain('is-active');
	const { menu: menu2 } = setup({ menuActive: false });
	expect(menu2.classList).not.toContain('is-active');
});

test('menuActive: controls is-active on burger', () => {
	const { burger: burger1 } = setup({ menuActive: true });
	expect(burger1.classList).toContain('is-active');
	const { burger: burger2 } = setup({ menuActive: false });
	expect(burger2.classList).not.toContain('is-active');
});

test('toggleMenu negates menuActive', () => {
	const host = { menuActive: false };
	toggleMenu(host);
	expect(host.menuActive).toBe(true);
	toggleMenu(host);
	expect(host.menuActive).toBe(false);
});

test('clicking on burger toggles menu', () => {
	const onToggleMenu = jest.fn();
	const { burger } = setup({ menuActive: true, onToggleMenu });
	burger.click();
	expect(onToggleMenu).toHaveBeenCalled();
});

function setup(host) {
	const { el, ...rest } = render(BNavbar, host);
	const nav = el.querySelector('nav');
	const burger = el.querySelector('.navbar-burger');
	const menu = el.querySelector('.navbar-menu');
	return { el, ...rest, burger, menu, nav };
}
